#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "util.h"

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    videoCapture = new cv::VideoCapture("rtsp://admin:admin@192.168.0.206:554");

    if (videoCapture->isOpened()) {
        qDebug() << "cam opened";
        *videoCapture >> currentImage;
        videoWriter = new cv::VideoWriter("/home/mbazs/img/video.avi", cv::VideoWriter::fourcc('H', '2', '6', '4'), 25, cv::Size(currentImage.rows, currentImage.cols));
        timer = new QTimer();
        connect(timer, SIGNAL(timeout()), this, SLOT(renderFrame()));
        timer->start(40);
    } else {
        exit(1);
    }
}

void MainWindow::renderFrame() {
    *videoCapture >> currentImage;
    if (!currentImage.empty()) {
        /*cv::Mat monoImage;
        cv::Mat grayImage;
        cv::cvtColor(currentImage, grayImage, cv::COLOR_RGB2GRAY);
        GaussianBlur(grayImage, grayImage, cv::Size(5,5), 0, 0);
        cv::threshold(grayImage, monoImage, 127, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);*/

        equalize();
        Util::setLabelImage(currentImage, ui->imageLabel, QImage::Format::Format_RGB888);

        //videoWriter->write(monoImage);
    }
}

void MainWindow::equalize() {
    cv::cvtColor(currentImage, currentImage, cv::COLOR_YCrCb2BGR);
    std::vector<cv::Mat> channels;
    cv::split(currentImage, channels);
    cv::equalizeHist(channels[0], channels[0]);
    cv::merge(channels, currentImage);
    cv::cvtColor(currentImage, currentImage, cv::COLOR_YCrCb2BGR);
}

MainWindow::~MainWindow()
{
    videoWriter->release();
    QThread::sleep(1);
    delete timer;
    QThread::sleep(1);
    delete videoCapture;
    QThread::sleep(1);
    delete ui;
}
