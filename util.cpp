#include <util.h>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

QImage Util::fromImage(const std::string &file) {
    cv::Mat m = cv::imread(file);
    cv::cvtColor(m, m, cv::COLOR_BGR2RGB);
    cv::Mat m2;
    cv::resize(m, m2, cv::Size(0, 0), 0.7, 0.7, cv::INTER_AREA);
    return QImage(m2.data, m2.cols, m2.rows, static_cast<int>(m2.step), QImage::Format_RGB888).copy();
}

void Util::setLabelImage(const std::string &file, QLabel *label) {
    label->setPixmap(QPixmap::fromImage(Util::fromImage(file)));
}

void Util::setLabelImage(const cv::Mat &image, QLabel *label, QImage::Format format) {
    label->setPixmap(QPixmap::fromImage(QImage(image.data, image.cols, image.rows, static_cast<int>(image.step), format).copy()));
}

cv::Mat Util::loadImageToMat(const std::string &file) {
    cv::Mat m = cv::imread(file);
    if (!m.empty()) {
        cv::cvtColor(m, m, cv::COLOR_BGR2RGB);
    }
    return m;
}
