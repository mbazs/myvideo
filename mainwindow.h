#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void renderFrame();

private:
    void equalize();

    Ui::MainWindow *ui;
    QTimer *timer;
    cv::VideoCapture *videoCapture;
    cv::VideoWriter *videoWriter;
    cv::Mat currentImage;
};

#endif // MAINWINDOW_H
