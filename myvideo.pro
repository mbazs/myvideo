#-------------------------------------------------
#
# Project created by QtCreator 2019-08-17T21:19:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = opencv_graymono
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++11

SOURCES += \
        camthread.cpp \
        main.cpp \
        mainwindow.cpp \
        util.cpp

HEADERS += \
        camthread.h \
        mainwindow.h \
        util.h

linux {
    INCLUDEPATH += /usr/local/include/opencv4
    LIBS += -lopencv_highgui -lopencv_core -lopencv_imgproc -lopencv_imgcodecs -lopencv_video -lopencv_videoio
}

win32 {
    INCLUDEPATH += c:\opencv\build\include
    LIBS += -lc:\libs\opencv_world411d
}

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


