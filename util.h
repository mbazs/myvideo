#ifndef UTIL_H
#define UTIL_H

#include <QtWidgets>
#include <string>
#include <opencv2/core.hpp>

class Util {
public:
    static QImage fromImage(const std::string &file);
    static void setLabelImage(const std::string &file, QLabel *label);
    static void setLabelImage(const cv::Mat &file, QLabel *label, QImage::Format format);
    static cv::Mat loadImageToMat(const std::string &file);
};

#endif // UTIL_H
